package xoxo;

import java.awt.event.ActionListener;
import javax.swing.*;
import java.awt.*;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Adiva Reyhan Puteri
 */
public class XoxoView {
    
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    
    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    /**
     * the GUI's frame
     */
    private JFrame frame;

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        //TODO: Construct your GUI here
        frame = new JFrame("Secret Message");
        frame.setLayout(new BorderLayout());
        frame.setSize(600,600);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        JPanel layout = new JPanel(new GridLayout(3,1));

        JPanel message = new JPanel(new FlowLayout());
        messageField = new JTextField();
        messageField.setPreferredSize(new Dimension(200,25));
        JLabel messageLabel = new JLabel("Message:");
        message.add(messageLabel);
        message.add(messageField);

        JPanel key = new JPanel(new FlowLayout());
        keyField = new JTextField();
        keyField.setPreferredSize(new Dimension(200,25));
        JLabel keyLabel = new JLabel("Key:       ");
        key.add(keyLabel);
        key.add(keyField);

        JPanel seed = new JPanel(new FlowLayout());
        seedField = new JTextField();
        seedField.setPreferredSize(new Dimension(200,25));
        JLabel seedLabel = new JLabel("Seed:     ");
        seed.add(seedLabel);
        seed.add(seedField);

        layout.add(message);
        layout.add(key);
        layout.add(seed);

        JPanel buttons = new JPanel(new GridLayout(1,2));
        encryptButton = new JButton("Encrypt");
        decryptButton = new JButton("Decrypt");
        encryptButton.setPreferredSize(new Dimension(150,20));
        decryptButton.setPreferredSize(new Dimension(150,20));
        buttons.add(encryptButton);
        buttons.add(decryptButton);

        logField = new JTextArea("Log :\n");
        logField.setPreferredSize(new Dimension(300,300));

        frame.add(layout, BorderLayout.PAGE_START);
        frame.add(buttons, BorderLayout.CENTER);
        frame.add(logField, BorderLayout.PAGE_END);
        frame.pack();
        frame.setVisible(true);

    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     * 
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * A method to show a notification when invalid input are used
     * @param mssg message to warn the user
     */

    public void notification(String mssg){
        JOptionPane.showMessageDialog(frame, mssg);
    }
    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }
}