package xoxo;
import xoxo.crypto.*;
import xoxo.exceptions.*;
import xoxo.util.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.awt.*;


import static xoxo.key.HugKey.DEFAULT_SEED;


/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Adiva Reyhan Puteri
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * An integer for naming the encyption files
     */
    private int encFile = 1;

    /**
     * An integer for naming the decryption files
     */
    private int decFile = 1;

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        gui.setEncryptFunction(actions -> encryption());
        gui.setDecryptFunction(actions -> decryption());

    }

    /**
     * A Method to encypt the original message
     */
    public void encryption(){
        String message = gui.getMessageText();
        String kissKey = gui.getKeyText();
        String seed=gui.getSeedText();
        XoxoEncryption encryptedMssg = null;

        try{
            encryptedMssg = new XoxoEncryption(kissKey);
        }
         catch(InvalidCharacterException e){
            gui.notification("Message should only contains letters and '@' symbol");
            return;
        }
        catch(KeyTooLongException e ){
            gui.notification("Key length exceede the maximum lenght");
            return;
        }
        XoxoMessage finalMessage = null;
        try{
            if(seed.equals("")||Integer.parseInt(seed)==18){
                finalMessage = encryptedMssg.encrypt(message);
                seed = "DEFAULT_SEED";
            }
            else{
                finalMessage = encryptedMssg.encrypt(message, Integer.parseInt(seed));
            }
        }
        catch(SizeTooBigException e){
            gui.notification("The message size exceeded the maximum size");
            return;
        }
        catch(RangeExceededException e ){
            gui.notification("Seed exceeded the maximum range");
            return;
        }
        catch(NumberFormatException e){
            gui.notification("Seed input must be a number");
            return;
        }
        encryptFile(finalMessage, seed);
    }

    /**
     * A method to write the encypted message to a file
     * @param mssg the encrypted message
     * @param seed the seed that are used
     */
    public void encryptFile(XoxoMessage mssg, String seed){
        String path = "//Users//Adiva Reyahn Putri//Desktop//Lab//lab_10//src//main//java//enc//" + encFile + ".enc";
        File newFile = new File(path);
        try{
            newFile.createNewFile();
            FileWriter writer = new FileWriter(newFile);
            writer.write(mssg.getEncryptedMessage());
            writer.flush();
            writer.close();
            gui.appendLog("===================");
            gui.appendLog("Encrypted Message: "+mssg.getEncryptedMessage());
            gui.appendLog("Key: "+mssg.getHugKey().getKeyString());
            gui.appendLog("Seed: "+ seed);
            gui.appendLog("\n");
            encFile++;
        }
        catch(IOException e){
            gui.appendLog("Encryption failed");
        }
    }

    /**
     * A method to decrypt the encrypted message
     */
    public void decryption(){
        String message = gui.getMessageText();
        String hugKey = gui.getKeyText();
        int seed;
        XoxoDecryption decryptedMssg = new XoxoDecryption(hugKey);
        if(gui.getSeedText().equals("")||Integer.parseInt(gui.getSeedText())==18){
            seed = DEFAULT_SEED;
        }
        else{
            seed = Integer.parseInt(gui.getSeedText());
        }
        String finalMessage = decryptedMssg.decrypt(message, seed);
        decryptFile(finalMessage);
    }

    /**
     * a Method to write the decrypted message to a file
     * @param mssg the decrypted message
     */
    public void decryptFile(String mssg){
        String path = "//Users//Adiva Reyahn Putri//Desktop//Lab//lab_10//src//main//java//dec//" + decFile + ".txt";
        File newFile = new File(path);
        try{
        newFile.createNewFile();
        FileWriter writer = new FileWriter(newFile);
        writer.write(mssg);
        writer.flush();
        writer.close();
        gui.appendLog("===================");
        gui.appendLog("Decrypted Message: "+mssg);
        gui.appendLog("\n");
        decFile++;
        }
        catch(IOException e){
            gui.appendLog("Decryption failed");
        }
    }

}