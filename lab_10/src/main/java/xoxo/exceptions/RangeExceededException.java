package xoxo.exceptions;

/**
 * An exception that is thrown if the seed
 *is not in between 0-36(inclusive)
 */
public class RangeExceededException extends RuntimeException{

    //TODO: Implement the exception
    /**
    *Class constructor
    */
    public RangeExceededException(String message){
    	super(message);
    }

}