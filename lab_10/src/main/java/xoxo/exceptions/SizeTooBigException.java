package xoxo.exceptions;

/**
 * An exception that is thrown if the message
 *exceed 10Kbit
 */
public class SizeTooBigException extends RuntimeException{

    //TODO: Implement the exception

    /**
    *Class constructor
    */
    public SizeTooBigException(String message){
    	super(message);
    }
    
}