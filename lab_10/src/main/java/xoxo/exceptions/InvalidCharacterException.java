package xoxo.exceptions;

/**
 * An exception that is thrown if the Kiss Key that
 * is used to encrypt a message contains letters beside
 *A-Z, a-z, and '@'
 */
public class InvalidCharacterException extends RuntimeException{

    //TODO: Implement the exception

    /**
    *Class constructor
    */
    public InvalidCharacterException(String message){
    	super(message);
    }
}