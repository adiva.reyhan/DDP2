package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.math.BigInteger;
import java.util.GregorianCalendar;
import java.util.ArrayList;

/**
* Class representing event managing system
*/
public class EventSystem
{
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;
    
    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem()
    {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }
    
    /**
    *method to check if an event is already initialized or not
    *@param name of the event
    *@return Event that has already been initialized, null if the event hasn't been initialized
    */
    public Event getEvent(String name){
        for (Event e : events) {
            if (e.getName().equals(name)) return e;
        }
        return null;
    }

    /** 
    *method to add an event
    *@param name of the event
    *@param startTimeStr strings of when an event starts
    *@param endTimeStr strings of when an event ends
    *@param costPerHourStr Strings of the event's cost
    *@return String whether the event is successfully added or not
    */

    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr)
    {
        // TODO: Implement!
        Event status = getEvent(name);
        if(status!=null) return "Event "+name+" sudah ada!";

        String[] parts = startTimeStr.split("_");
        String[] parts2 = endTimeStr.split("_");

        String[] detailStart = startTimeStr.split("-|\\_|\\:");
        String[] detailEnd = endTimeStr.split("-|\\_|\\:");

        GregorianCalendar start = new GregorianCalendar(Integer.parseInt(detailStart[0]),Integer.parseInt(detailStart[1])-1,Integer.parseInt(detailStart[2]),Integer.parseInt(detailStart[3]),Integer.parseInt(detailStart[4]),Integer.parseInt(detailStart[5]));
        GregorianCalendar end = new GregorianCalendar(Integer.parseInt(detailEnd[0]),Integer.parseInt(detailEnd[1])-1,Integer.parseInt(detailEnd[2]),Integer.parseInt(detailEnd[3]),Integer.parseInt(detailEnd[4]),Integer.parseInt(detailEnd[5]));

        BigInteger cost = new BigInteger(costPerHourStr);

        Event acara = new Event(name, start, end, cost);

        if (start.compareTo(end)>0) return "Waktu yang diinputkan tidak valid!";

        events.add(acara);
        return "Event " + name +" berhasil ditambahkan!";
    }
    
    /** 
    *method to check whether the user has already been initialized
    *@param name of the user
    *@return User that has already been initialized, null if not found
    */
    public User getUser(String name){
        for(User u : users){
            if (u.getName().equals(name)) return u;
        }
        return null;
    }

    /**
    *method to add user
    *@param name of the user that we want to add
    *@return string whether the user is successfully added or not
    */
    public String addUser(String name)
    {
        User stats = getUser(name);
        if(stats != null) return "User "+ name+" sudah ada!";

        User person = new User(name);
        users.add(person);


        return "User "+name+" berhasil ditambahkan!";
    }

    /**
    *method for a user to register to an event
    *@param userName that wants to be registered
    *@param eventName that wants to be registered
    *@return String whether the user is succesfully registered to the event
    */
    public String registerToEvent(String userName, String eventName)
    {
        User statUser = getUser(userName);
        Event statEvent = getEvent(eventName);

        if(statUser!= null || statEvent != null){
            if(statUser!= null){
                if(statEvent!=null){
                    if(statUser.addEvent(statEvent)){
                        return userName+" berencana menghadiri "+eventName+"!";
                    }
                    return userName+" sibuk sehingga tidak dapat menghadiri "+ eventName+"!";
                }
                return "Tidak ada acara dengan nama "+eventName+"!";
            }
            return "Tidak ada pengguna dengan nama "+userName+"!";
        }
        return "Tidak ada pengguna dengan nama "+userName+" dan acara dengan nama "+eventName+"!";

    }

}