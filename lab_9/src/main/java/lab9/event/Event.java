package lab9.event;

import lab9.user.User;
import java.util.GregorianCalendar;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
/**
* A class representing an event and its properties
*/
public class Event implements Comparable<Event>
{
    /** Name of event */
    private String name;

    /** When the  event started and ended*/

    private GregorianCalendar start;
    private GregorianCalendar end;

    //** The cost of the event per hour*/
    private BigInteger cost;

    /**
     * constructor
     * @param name of the event
     * @param start of the event
     * @param end of the event
     * @param cost per hour of the event
     */
    public Event(String name, GregorianCalendar start, GregorianCalendar end, BigInteger cost){
    this.name=name;
    this.start=start;
    this.end=end;
    this.cost=cost;
    }
    
    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    public String getName()
    {
        return this.name;
    }

    /**
    *Accessor for cost
    *@return BigInteger of the event's cost
    */
    public BigInteger getCost(){
        return this.cost;
    }
    
    /**
    *Method to print the information of the event
    *@return String of the event's information
    */
    public String toString(){
        SimpleDateFormat dates = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");

        return this.name + "\n"+
                "Waktu mulai: "+dates.format(this.start.getTime())+"\n"+
                "Waktu selesai: "+dates.format(this.end.getTime())+"\n"+
                "Biaya kehadiran: " +this.cost;
    }

    /**
    *Method to copy existed events
    *@return the copied event
    */
    public Event copyEvent(){
        Event copy = new Event(this.name, this.start, this.end, this.cost);
        return copy;
    }


    public boolean overlapsWith(Event other){
    // HINT: Implement a method to test if this event overlaps with another event
    //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
    //       with other parts of the implementation.
        if(start.compareTo(other.start)<0 && end.compareTo(other.end)<0) return false;
        else if (other.start.compareTo(start)<0 && other.end.compareTo(start)<0) return false;
        else if(other.end.compareTo(start)==0 || end.compareTo(other.start)==0) return false;
        return true;
    }

    /**
     *Method overriding the compareTo method on the Comparable Interface
     *@param other is an event that "this" is compared to
     *@return -1 when this is earlier than other, 0 when other is equal, 1 when other is later
     */
    @Override
    public int compareTo(Event other) {
        return start.compareTo(other.start);
    }

}
