import java.util.ArrayList;

public class Manusia{

	// atribute
	private String nama;
	private int umur;
	private int uang;
	private float kebahagiaan=50;
	private final static float MAXBAHAGIA = 100;
	private final static float MINBAHAGIA = 0;
	private static ArrayList<Manusia> listNama = new ArrayList<Manusia>();
	private boolean meninggal;

	// konstruktor
	public Manusia(String nama, int umur, int uang, float kebahagiaan){
		this.nama=nama;
		this.umur=umur;
		this.uang=uang;
		this.kebahagiaan=kebahagiaan;
		this.meninggal=false;
		listNama.add(0,this);
	}

	public Manusia(String nama, int umur, float kebahagiaan){
		this.nama=nama;
		this.umur=umur;
		this.uang=50000;
		this.kebahagiaan=kebahagiaan;
		this.meninggal=false;
		listNama.add(0,this);
	}

	public Manusia (String nama, int umur){
		this.nama=nama;
		this.umur=umur;
		this.uang=50000;
		this.kebahagiaan=50;
		this.meninggal=false;
		listNama.add(0, this);
	}

	public Manusia (String nama,int umur, int uang){
		this.nama=nama;
		this.umur=umur;
		this.uang=uang;
		this.kebahagiaan=50;
		this.meninggal=false;
		listNama.add(0, this);
	}
	
	// setters and getters
	public String getNama(){
		return nama;
	}

	public void setNama(String nama){
		this.nama=nama;
	}

	public int getUmur(){
		return umur;
	}

	public void setUmur(int umur){
		this.umur=umur;
	}

	public int getUang(){
		return uang;
	}

	public void setUang(int uang){
		this.uang=uang;
	}

	public float getKebahagiaan(){
		return kebahagiaan;
	}

	public void setKebahagiaan(float kebahagiaan){
		if (kebahagiaan > MAXBAHAGIA){
			this.kebahagiaan=MAXBAHAGIA;
		}
		else if (kebahagiaan < MINBAHAGIA){
			this.kebahagiaan=MINBAHAGIA;
		}
		else{
		this.kebahagiaan=kebahagiaan;
		}
	}

	public boolean getMeninggal(){
		return meninggal;
	}

	public void setMeninggal(boolean meninggal){
		this.meninggal=meninggal;
	}

	// methods
	public void beriUang(Manusia penerima){
		if (meninggal==false){
			if (penerima.meninggal == false){
				int jumlahAscii=0;
				for(int a=0; a<penerima.nama.length(); a++){
					int ascii=(int) penerima.nama.charAt(a);
					jumlahAscii+=ascii;
				}
				int uangDiberi = jumlahAscii * 100;
				if (this.uang >= uangDiberi){
					this.setUang(this.getUang() - uangDiberi);
					this.setKebahagiaan(this.getKebahagiaan() +((float)uangDiberi/6000));
					penerima.setKebahagiaan(penerima.getKebahagiaan() + ((float)uangDiberi/6000));
					penerima.setUang(penerima.getUang() + uangDiberi);
					System.out.println(this.nama + " memberi uang sebanyak " + uangDiberi + " kepada " + penerima.nama +", mereka berdua senang :D");
				}
				else{
					System.out.println(this.nama + " ingin memberi uang kepada " + penerima.nama + " namun tidak memiliki cukup uang :'(");
				}
			}
			else{
				System.out.println(penerima.nama + " telah tiada");
			}
		}	
		else{
			System.out.println(nama+" telah tiada");
		}
	}
	public void beriUang(Manusia penerima, int jumlah){
		if (meninggal == false){
			if (penerima.meninggal == false){
				if (this.uang >= jumlah ){
					this.setUang(this.getUang() - jumlah );
					penerima.setUang(penerima.getUang() + jumlah);
					this.setKebahagiaan(this.getKebahagiaan() + ((float)jumlah/6000));
					if (this.kebahagiaan >= MAXBAHAGIA){
						this.setKebahagiaan(MAXBAHAGIA);
					}
					penerima.setKebahagiaan(penerima.getKebahagiaan() + ((float)jumlah/6000));
					if(penerima.kebahagiaan >= MAXBAHAGIA){
						penerima.setKebahagiaan(MAXBAHAGIA);
					}
					System.out.println(this.nama + " memberi uang sebanyak " + jumlah + " kepada "+ penerima.nama +",  mereka berdua senang :D");
				}
				else {
					System.out.println(this.nama + " ingin memberi uang kepada " + penerima.nama + " namun tidak memiliki cukup uang :'(");
				}
			}
			else{
				System.out.println(penerima.nama + " telah tiada");
			}
		}
		else{
			System.out.println(nama + " telah tiada");
		}
	}

	public void bekerja (int durasi, int bebanKerja){
		if (meninggal == false){
			if (this.umur < 18){
				System.out.println(this.nama + " belum boleh bekerja karena masih dibawah umur D:");
			}

			else {
				int bebanKerjaTotal=durasi * bebanKerja;
				int pendapatan=0;
				if (bebanKerjaTotal <= this.kebahagiaan){
					this.setKebahagiaan(this.getKebahagiaan() - bebanKerjaTotal);
					pendapatan = bebanKerjaTotal * 10000;
					System.out.println(this.nama+" bekerja full time, total pendapatan : "+ pendapatan);
				}
				else if (bebanKerjaTotal > this.kebahagiaan){
					int durasiBaru =(int) this.kebahagiaan/bebanKerja;
					bebanKerjaTotal=durasiBaru * bebanKerja;
					pendapatan = bebanKerjaTotal * 10000;
					this.setKebahagiaan(this.getKebahagiaan()-bebanKerjaTotal);
					System.out.println(this.nama + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : "+ pendapatan);
				}
				this.setUang(this.getUang()+ pendapatan);
			}
		}
	else{
		System.out.println(nama + " telah tiada");
	}
}

	public void rekreasi(String namaTempat){
		if (meninggal==false){
			int biaya = (int) namaTempat.length() * 10000;
			if(this.uang > biaya){
				this.setUang(this.getUang()-biaya);
				this.setKebahagiaan(this.getKebahagiaan() + namaTempat.length());
				System.out.println(this.nama + " berekreasi di " + namaTempat + ", "+this.nama+ " senang :)");
			}
			else{
				System.out.println(this.nama + " tidak mempunyai cukup uang untuk berekreasi di "+ namaTempat + ":(");
			}
		}
		else{
			System.out.println(nama+ " telah tiada");
		}
	}

	public void sakit(String namaPenyakit){
		if (meninggal==false){
			this.setKebahagiaan(this.getKebahagiaan()-namaPenyakit.length());
			System.out.println(this.nama + " terkena penyakit "+ namaPenyakit + " :O");
		}
		else{
			System.out.println(nama+" telah tiada");
		}
	}

	public void meninggal(){
		if (meninggal==true){
			System.out.println(nama+" telah tiada");
		}
		else{
			System.out.println(nama+" meninggal dengan tenang, kebahagiaan: "+ kebahagiaan);
			Manusia manusiaTerakhir = listNama.get(0);
			if (manusiaTerakhir.equals(this)){
				System.out.println("Semua harta "+ nama + " hangus");
			}
			else if (manusiaTerakhir.meninggal==false){
				manusiaTerakhir.setUang(manusiaTerakhir.getUang()+this.getUang());
				setUang(0);
				System.out.println("Semua harta "+nama+" disumbangkan untuk "+manusiaTerakhir.nama);
				this.meninggal=true;
				this.nama = "Almarhum " + this.nama;
			}

			else {
				System.out.println("Semua harta "+ manusiaTerakhir.nama + " hangus");
			}
		}
	}

	/*public String toString(){
			return ("Nama\t\t: " + this.nama + 
				"\nUmur\t\t: "+ this.umur +
				"\nUang\t\t: "+this.uang+ 
				"\nKebahagiaan\t: "+this.kebahagiaan);
		}*/
	}	