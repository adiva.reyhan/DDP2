/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */

public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
	
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	

	public boolean isBingo() {
		for (int a=0; a<5; a++){
			if(numbers[a][0].isChecked()&&
				numbers[a][1].isChecked()&&
				numbers[a][2].isChecked()&&
				numbers[a][3].isChecked()&&
				numbers[a][4].isChecked()){
				isBingo=true;
				break;
			}
			else if (numbers[0][a].isChecked()&&
				numbers[1][a].isChecked()&&
				numbers[2][a].isChecked()&&
				numbers[3][a].isChecked()&&
				numbers[4][a].isChecked()){
				isBingo=true;
				break;
			}
		}
		if (numbers[0][0].isChecked()&&
			numbers[1][1].isChecked()&&
			numbers[2][2].isChecked()&&
			numbers[3][3].isChecked()&&
			numbers[4][4].isChecked()){
			isBingo=true;
		}
		else if (numbers[0][4].isChecked()&&
			numbers[1][3].isChecked()&&
			numbers[2][2].isChecked()&&
			numbers[3][1].isChecked()&&
			numbers[4][0].isChecked()){
			isBingo=true;
		}
		if (isBingo){
			System.out.println("BINGO!");
			System.out.println(info());
		}

		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}


	public String markNum(int num){
		//TODO Implement
		if (numberStates[num] == null){
			return "Kartu tidak memiliki angka " + num;
		}
		else{
		
			if (numberStates[num].isChecked()){
				return num + " sebelumnya sudah tersilang";
			}

			else{
				numberStates[num].setChecked(true);
				return num + " tersilang" ;
			}
		}
		
	}	

	
	public String info(){
		//TODO Implement
		String tabel="";
		for (int kolom=0; kolom <5; kolom++){
			for (int baris=0; baris <5; baris++){
				if (numbers[kolom][baris].isChecked()==false){
					tabel +="| "+ numbers[kolom][baris].getValue()+" " ;
				}
				else{
					tabel+= "| X  ";
				}

			}
			if (kolom != 4){
				tabel+="|\n";
			}
			else{
				tabel+="|";
			}
		}
		return tabel;
	}
	
	public void restart(){
		//TODO Implement
		System.out.println("Mulligan!");
		for (int column=0; column<5; column++){
			for(int row=0; row<5; row++){
				numbers[column][row].setChecked(false);
			}
		}

	}
	

}
