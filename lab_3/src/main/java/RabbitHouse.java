import java.util.Scanner;

public class RabbitHouse {
	public static int kembangBiak(int panjangNama , int jumlahKelinci ){
		if(panjangNama == 1) return 1;
		else {
			jumlahKelinci = panjangNama *jumlahKelinci;
			return jumlahKelinci + kembangBiak((panjangNama-1),jumlahKelinci);}
		}

	public static boolean cekPalindrom (String namaKelinci){
		String reverseNama = new StringBuffer(namaKelinci).reverse().toString();
		if (reverseNama.equals(namaKelinci)){
			return true;
		}
		else{
			return false;
		}
	}

	public static int palindrom (String namaKelinci){
		int count=0;
		if (namaKelinci.length()==1){
			return count;
		}
		else {
			if (cekPalindrom(namaKelinci)==true){
				return count;
			}
			else {
				count+=1;
				for (int a=0; a<namaKelinci.length(); a++){
					String namaBaru=namaKelinci.substring(0,a) + namaKelinci.substring(a+1);
					if (cekPalindrom(namaBaru)==false){
						count+= palindrom(namaBaru);
					}

				}
			}
			return count;
		}
	}
	public static void main (String[] args){
		Scanner masukkan = new Scanner(System.in);
		String inputKelinci= masukkan.nextLine();
		String[] hasilSplit = inputKelinci.split(" ");


		if (hasilSplit.length != 2){
			System.out.println("Input Salah");
			System.exit(0);
		}

		else if (hasilSplit[1].length() > 10){
			System.out.println("Input Salah");
			System.exit(0);
		}

		int panjangNama= hasilSplit[1].length();
		int jumlahKelinci=1;

		if (hasilSplit[0].toLowerCase().equals("normal")){
			System.out.println(kembangBiak(panjangNama, jumlahKelinci));
		}

		else if (hasilSplit[0].toLowerCase().equals("palindrom")){
			System.out.println(palindrom(hasilSplit[1]));

		}
		else {
			System.out.println("Mode tidak tersedia");
		}
	}
}
