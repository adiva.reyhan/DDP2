import karyawan.*;
import java.util.*;

class Lab8 {

    static private ArrayList<Karyawan> karyawan = new ArrayList<Karyawan>();
    static private int limit=18000;

	public static Karyawan find(String name){
    		for(int i=0; i<karyawan.size(); i++){
				if(karyawan.get(i).getName().equals(name)){
					return karyawan.get(i);
				}
			}
			return null;
    	}

    public static void main(String[] args) {
    	Scanner input = new Scanner(System.in);
    	while(true){
    		String command = input.nextLine();
    		if(command.matches("[0-9]+")) limit = Integer.parseInt(command);
    		else{
    			String[] info = command.split(" ");
    			if(info[0].equals("TAMBAH_KARYAWAN")){
    				String name = info[1];
    				int wage= Integer.parseInt(info[3]);
    				if(find(name)!=null){
    					System.out.println("Karyawan dengan nama tersebut sudah terdaftar");
    				}

    				else{
	    				if(info[2].equals("MANAGER")){
	    					Karyawan manage = new Manager(name, wage);
	    					karyawan.add(manage);
	    				}
	    				else if (info[2].equals("STAFF")){
	    					Karyawan staff = new Staff(name, wage);
	    					karyawan.add(staff);
	    				}

	    				else if(info[2].equals("INTERN")){
	    					Karyawan intern = new Intern(name, wage);
	    					karyawan.add(intern);
	    				}
	    				System.out.println(name +" mulai bekerja sebagai "+ info[2] + " di PT. TAMPAN");
    				}
    			}

    			else if (info[0].equals("GAJIAN")){
    				System.out.println("Semua karyawan telah diberikan gaji");
    				for(int i = 0; i<karyawan.size(); i++){
    					karyawan.get(i).gajian();
    					if(karyawan.get(i) instanceof Staff){
							if(karyawan.get(i).getWage()>limit){ //batas gaji staff
								Manager promote = new Manager(karyawan.get(i).getName(), karyawan.get(i).getWage());
								karyawan.remove(karyawan.get(i));
                                karyawan.add(i, promote);
								System.out.println("Selamat, "+ karyawan.get(i).getName()+" telah dipromosikan menjadi MANAGER");
								
							} 
    					}
			
					}
    
    			}

    			
    			else if(info[0].equals("STATUS")){
    				String name = info[1];
                    if(find(name)!=null){
                        System.out.println(find(name).status());
                    }
                    else {System.out.println("Karyawan tidak ditemukan");}
    			}

    			else if(info[0].equals("TAMBAH_BAWAHAN")){
    				Karyawan boss = find(info[1]);
    				Karyawan underling = find(info[2]);

    				if(boss!=null && underling !=null){
    					if (boss instanceof Manager){
    						Manager realBoss = (Manager) boss;
    						System.out.println(realBoss.addBawahan(underling));
    					}
    					else if (boss instanceof Staff){
    						Staff realBoss = (Staff) boss;
    						System.out.println(realBoss.addBawahan(underling));
    					}
                        else{
                            Intern realBoss = (Intern) boss;
                            System.out.println(realBoss.addBawahan(underling));
    					}
    				}
                    else{
    				System.out.println("Nama tidak berhasil ditemukan");}
    			}

    			else if (info[0].equals("SELESAI")) {
    				break;
    			}
    		}
    	}
        input.close();
    }

}