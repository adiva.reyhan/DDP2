package karyawan;

public abstract class Karyawan{
	protected String name;
	protected String type;
	protected int wage;
	protected int wageCount;
	
	public Karyawan(String name, int wage){
		this.name=name;
		this.wage=wage;
		this.type="";
	}

	public String getName(){
		return name;
	}

	public String getType(){
		return type;
	}

	public int getWage(){
		return wage;
	}

	public void setWage(int wage){
		this.wage=wage;
	}

	public int getWageCount(){
		return wageCount;
	}

	public void gajian(){
		wageCount++;
		if(wageCount>=6){
			int promote = (int) (this.getWage()*0.1);
			int oldWage = this.getWage();
			this.setWage(this.getWage()+ promote);
			wageCount=0;
			System.out.println(this.name+" mengalami kenaikan gaji sebesar 10% dari "+oldWage+" menjadi "+this.getWage());
		}

	}
	
	public String status(){
		return ""+this.getName()+" "+this.getWage();
	}

	public abstract String addBawahan(Karyawan other);
	
}