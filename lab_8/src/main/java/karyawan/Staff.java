package karyawan;
import java.util.*;

public class Staff extends Karyawan{
	private ArrayList<Karyawan> underling = new ArrayList<Karyawan>();

	public Staff(String name, int wage){
		super(name, wage);
		this.type="Staff";
	}

	public ArrayList<Karyawan> getUnderling(){
		return underling;
	}

	@Override
	public String addBawahan(Karyawan other){
		for(int i = 0; i<underling.size(); i++){
			if(underling.get(i).getName().equals(other.getName())){
				return "Karyawan "+other.getName()+" telah ditambahkan menjadi bawahan "+ this.getName();
			}
		}
		if(other instanceof Intern){
			if (this.getUnderling().size() == 8) {				
				return getName()+" sudah memiliki 8 bawahan";
			}
			else if(this.getUnderling().size()<=8){
				this.getUnderling().add(other);
				return "Karyawan "+ other.getName()+" berhasil ditambahkan menjadi bawahan "+this.getName();
			}
		}
		return "Anda tidak layak memiliki bawahan";
	}
}