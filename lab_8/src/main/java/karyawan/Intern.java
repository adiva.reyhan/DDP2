package karyawan;

public class Intern extends Karyawan{
	public Intern  (String name, int wage){
		super(name, wage);
		this.type="Intern";
	}

	@Override
	public String addBawahan(Karyawan other){
		return "Anda tidak layak memiliki bawahan";
	}
}