package karyawan;
import java.util.*;

public class Manager extends Karyawan{
	private ArrayList<Karyawan> underling = new ArrayList<Karyawan>();

	public Manager(String name, int wage){
		super(name, wage);
		this.type="Manager";
		this.underling=underling;
	}

	public ArrayList<Karyawan> getUnderling(){
		return underling;
	}

	@Override
	public String addBawahan(Karyawan other){
		for(int i = 0; i<underling.size() ; i++){
			if(this.getUnderling().get(i).getName().equals(other.getName())){
				return "Karyawan "+other.getName()+" telah ditambahkan menjadi bawahan "+ this.getName();
			}
		}
		if(other instanceof Staff){
			if (this.getUnderling().size() == 10) {				
				return getName()+" sudah memiliki 10 bawahan";
			}
			else if(this.getUnderling().size()<=10){
				this.getUnderling().add(other);
				return "Karyawan "+ other.getName()+" berhasil ditambahkan menjadi bawahan "+this.getName();
			}
		}
		else if(other instanceof Intern){
			if (this.getUnderling().size() == 10) {				
				return getName()+" sudah memiliki 10 bawahan";
			}
			else if(this.getUnderling().size()<=10){
				this.getUnderling().add(other);
				return "Karyawan "+ other.getName()+" berhasil ditambahkan menjadi bawahan "+this.getName();
			}
		}
		return "Anda tidak layak memiliki bawahan";
	}
}