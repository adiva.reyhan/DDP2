import character.*;

import java.util.ArrayList;

public class Game {
    ArrayList<Player> player = new ArrayList<Player>();
    ArrayList<Player> eatenGame = new ArrayList<Player>();

    /**
     * Fungsi untuk mencari karakter
     *
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name) {
        for (int i = 0; i < player.size(); i++) {
            if (name.equals(player.get(i).getName())) return player.get(i);
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     *
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int    hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp) {

        //klo hasil find nama nya true berarti udh exist klo blm baru add biasa
        if (find(chara) != null) {
            System.out.print("Sudah ada karakter bernama " + chara);
        } else {
            if (tipe.equals("Human")) {
                Player manusia = new Human(chara, hp);
                player.add(manusia);
            } else if (tipe.equals("Magician")) {
                Player penyihir = new Magician(chara, hp);
                player.add(penyihir);
            } else {
                Player mons = new Monster(chara, hp);
                player.add(mons);
            }
            return chara + " ditambah ke game";
        }
        return "";
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     *
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int    hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar) {
        if (find(chara) != null) {
            return "Sudah ada karakter bernama " + chara;
        } else {
            Player mon = new Monster(chara, hp, roar);
            player.add(mon);
        }
        return chara + " ditambah ke game";
    }

    /**
     * fungsi untuk menghapus character dari game
     *
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara) {
        if (find(chara) == null) return "Tidak ada " + chara;
        player.remove(find(chara));
        return chara + " dihapus dari game";
    }

    /**
     * fungsi untuk menampilkan status character dari game
     *
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara) {
        String hidup = "";
        String riwayatDiet = "";
        //String early=""; ini buat urutan yg ke add duluan
        Player self = find(chara);

        if (self != null) {
            if (self.isDead()) hidup = "Sudah meninggal dunia dengan damai";
            else {
                hidup = "Masih hidup";
            }

            
            if (self.getDiet().size() == 0) riwayatDiet = "Belum memakan siapa siapa";
            else {
                riwayatDiet = "Memakan " + diet(chara);
            }
            return "" + self.getType() + " " + self.getName() + "\n" +
                    "HP: " + self.getHp() + "\n" +
                    hidup + "\n" +
                    riwayatDiet;
        }
        return "Tidak ada " + chara;
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     *
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status() {
        String hidup = "";
        String riwayatDiet = "";
        String finalRiwayatDiet = "";
        String allStatus = "";

        for (int i = 0; i < player.size(); i++) {
            if (player.get(i).getHp() == 0) player.get(i).setDead(true);
            if (player.get(i).isDead()) hidup = "Sudah meninggal dunia dengan damai";
            else {
                hidup = "Masih hidup";
            }

            riwayatDiet = diet(player.get(i).getName());
            if (riwayatDiet == "") finalRiwayatDiet = "Belum memakan siapa siapa";
            else {
                finalRiwayatDiet = "Memakan " + riwayatDiet;
            }
            if (i == player.size() - 1) {
                allStatus = allStatus + player.get(i).getType() + " " + player.get(i).getName() + "\n" +
                        "HP: " + player.get(i).getHp() + "\n" +
                        hidup + "\n" +
                        finalRiwayatDiet;
            } else {
                allStatus = allStatus + player.get(i).getType() + " " + player.get(i).getName() + "\n" +
                        "HP: " + player.get(i).getHp() + "\n" +
                        hidup + "\n" +
                        finalRiwayatDiet + "\n";
            }

        }

        return allStatus;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     *
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara) {
        String riwayatDiet = "";
        Player self;

        if (find(chara) != null) {
            self = find(chara);
            for (int i = 0; i < self.getDiet().size(); i++) {
                if (i == self.getDiet().size() - 1)
                    riwayatDiet += self.getDiet().get(i).getType() + " " + self.getDiet().get(i).getName();
                else {
                    riwayatDiet += self.getDiet().get(i).getType() + " " + self.getDiet().get(i).getName();
                }
            }
            return riwayatDiet;
        }
        return "Tidak ada " + chara;
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     *
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet() {
        String riwayatDiet = "";

        for (int i = 0; i < eatenGame.size(); i++) {
            if (i == eatenGame.size() - 1) riwayatDiet += eatenGame.get(i).getType() + " " + eatenGame.get(i).getName();
            else {
                riwayatDiet += eatenGame.get(i).getType() + " " + eatenGame.get(i).getName();
            }

        }
        return "";

    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     *
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName) {
        Player self = find(meName);
        Player enemy = find(enemyName);

        if (!self.isDead()) {
            if (enemy instanceof Magician) {
                enemy.setHp(enemy.getHp() - 10 * 2);
                if (enemy.getHp() <= 0) {
                    enemy.setHp(0);
                    enemy.setDead(true);
                    return "Nyawa " + enemy.getName() + " " + enemy.getHp();
                }
                return "Nyawa " + enemy.getName() + " " + enemy.getHp();
            } else {
                enemy.setHp(enemy.getHp() - 10);
                if (enemy.getHp() <= 0) {
                    enemy.setHp(0);
                    enemy.setDead(true);
                    return "Nyawa " + enemy.getName() + " " + enemy.getHp();
                }
                return "Nyawa " + enemy.getName() + " " + enemy.getHp();
            }
        }
        return self.getName() + " tidak bisa menyerang " + enemy.getName();
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     *
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName) {
        Player self = find(meName);
        Player enemy = find(enemyName);

        if (self != null) {
            if (enemy != null) {
                if (self.isDead() == false) {
                    if (self instanceof Magician){
                        Magician me = (Magician) self;
                        return me.burn(enemy);
                    }
                    return enemyName +" bukan Magician";
                }
                return self.getName() + " tidak bisa membakar " + enemy.getName();
            }
            return "Tidak ada " + meName + " atau " + enemyName;
        }
        return "Tidak ada " + meName + " atau " + enemyName;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     *
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName) {
        Player self = find(meName);
        Player enemy = find(enemyName);

        if (self != null) {
            if (enemy != null) {
                if (!self.isDead()) {
                    if (self.canEat(enemy)) {
                        self.setHp(self.getHp() + 15);
                        self.addEatenList(enemy);
                        eatenGame.add(enemy);
                        player.remove(enemy);
                        return meName + " memakan " + enemyName + "\n" +
                                "Nyawa " + meName + " kini " + self.getHp();
                    }
                    return meName + " tidak bisa memakan " + enemyName;
                }
                return meName + " tidak bisa memakan " + enemyName;
            }
            return "Tidak ada " + meName + " atau " + enemyName;
        }
        return "Tidak ada " + meName + " atau " + enemyName;
    }

    /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     *
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName) {
        Player self = find(meName);

        if (self != null) {
            if (!self.isDead()) {
                if (self instanceof Monster) {
                    Monster mons = (Monster) self;
                    return mons.roar();
                }
                return meName + " tidak bisa berteriak";
            }
            return "Tidak ada " + meName;
        }
        return "Tidak ada " + meName;
    }
}