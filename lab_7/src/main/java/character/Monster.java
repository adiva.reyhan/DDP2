package character;

import character.Player;

//  write Monster Class here
public class Monster extends Player {
    private String roar;

    public Monster(String name, int hp) {
        super(name, (hp * 2));
        this.roar = "AAAAAAaaaAAAAAaaaAAAAAA";
        this.type = "Monster";
    }

    public Monster(String name, int hp, String roar) {
        super(name, (hp * 2));
        this.roar = roar;
        this.type = "Monster";

    }

    public String roar() {
        return ("AAAAAAaaaAAAAAaaaAAAAAA");
    }

    public String getRoar() {
        return roar;
    }

    public void setRoar(String roar) {
        this.roar = roar;
    }

}