package character;

import character.Human;

//  write Magician Class here
public class Magician extends Human {

    public Magician(String name, int hp) {
        super(name, hp);
        this.type = "Magician";
    }

    public String burn(Player enemy) {
        if (enemy instanceof Magician) {
            enemy.setHp(enemy.getHp() - 10 * 2);
            if (enemy.getHp() <= 0) {
                enemy.setHp(0);
                enemy.setDead(true);
                enemy.setBurn(true);
                return "Nyawa " + enemy.getName() + " " + enemy.getHp() + "\n dan matang";
            }
            return "Nyawa " + enemy.getName() + " " + enemy.getHp();
        } else {
            enemy.setHp(enemy.getHp() - 10);
            if (enemy.getHp() <= 0) {
                enemy.setHp(0);
                enemy.setDead(true);
                enemy.setBurn(true);
                return "Nyawa " + enemy.getName() + " " + enemy.getHp() + "\n dan matang";
            }
            return "Nyawa " + enemy.getName() + " " + enemy.getHp();
        }
    }

    public String getType() {
        return this.type;
    }

}