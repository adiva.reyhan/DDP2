package character;

import java.util.ArrayList;

//  write Player Class here
public class Player {
    protected String name;
    protected int hp;
    protected ArrayList<Player> diet;
    protected boolean isDead;
    protected boolean isBurn;
    protected String type;


    public Player(String name, int hp) {
        this.name = name;
        this.hp = hp;
        this.diet = new ArrayList<Player>();
        this.isDead = (hp > 0) ? false : true;
        this.isBurn = false;
    }

    public String getName() {
        return name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public boolean isDead() {
        return isDead;
    }

    public void setDead(boolean dead) {
        isDead = dead;
    }

    public boolean isBurn() {
        return isDead;
    }

    public void setBurn(boolean burn) {
        isBurn = burn;
    }

    public ArrayList<Player> getDiet() {
        return this.diet;
    }


    public String getType() {
        return this.type;
    }


    public boolean canEat(Player enemy) {
        if (this instanceof Monster) {
            if (enemy.isDead()) {
                return true;
            }
            return false;
        } else {
            if (enemy instanceof Monster) {
                if (enemy.isDead()) {
                    if (enemy.isBurn()) {
                        return true;
                    }
                }

                return false;
            }
            return false;
        }

    }

    public void addEatenList(Player name) {
        diet.add(name);
    }

    public void attack(Player enemy) {
        if (enemy instanceof Magician) {
            enemy.setHp(enemy.getHp() - 10 * 2);
        } else {
            enemy.setHp(enemy.getHp() - 10);
        }
    }
}