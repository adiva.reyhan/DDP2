package character;

import character.Player;

//  write Human Class here
public class Human extends Player {

    public Human(String name, int hp) {
        super(name, hp);
        this.type = "Human";
    }

}