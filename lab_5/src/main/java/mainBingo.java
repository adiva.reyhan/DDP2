import java.util.Scanner;

public class mainBingo{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		Number[][] kolomBaris = new Number[5][5];
		Number[] cekAngka = new Number[100];
		String[][] baris= new String[5][5];

		for (int a=0; a<5 ; a++){
			String[] angka = input.nextLine().split(" ");
			baris[a] = angka;
		}

		for(int b=0; b<5; b++){
			for(int c=0; c<5; c++){
				kolomBaris[b][c] = new Number(Integer.parseInt(baris[b][c]) , b, c);
			}
		}

		for (int d=0; d<5; d++){
			for (int e=0; e<5; e++){
				cekAngka[kolomBaris[d][e].getValue()]=kolomBaris[d][e];
			}
		}

		BingoCard bingoSeru = new BingoCard(kolomBaris, cekAngka);

		while(bingoSeru.isBingo()!= true){
			String[] perintah= input.nextLine().split(" ");
			if (perintah[0].toLowerCase().equals("mark")){
				int num=Integer.parseInt(perintah[1]);
				System.out.println(bingoSeru.markNum(num));
			}
			else if (perintah[0].toLowerCase().equals("info")){
				System.out.println(bingoSeru.info());
			}
			else if (perintah[0].toLowerCase().equals("restart")){
				bingoSeru.restart();
			}
			else{
				System.out.println("Incorrect Command");
			}
		}
	}
}