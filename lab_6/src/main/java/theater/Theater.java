package theater;
import java.util.ArrayList;
import movie.Movie;
import ticket.Ticket;


public class Theater {
    private String nama;
    private int saldo;
    private ArrayList<Ticket> tiket;
    private Movie[] film;

    public Theater(String nama, int saldo, ArrayList<Ticket> tiket, Movie[] film ){
        this.nama=nama;
        this.saldo=saldo;
        this.tiket=tiket;
        this.film=film;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getSaldo() {
        return saldo;
    }
    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public Movie[] getFilm() {
        return film;
    }

    public void setFilm(Movie[] film) {
        this.film = film;
    }

    public ArrayList<Ticket> getTicket(){
        return tiket;
    }

    public void printInfo(){
        System.out.println("------------------------------------------------------------------");
        System.out.printf("Bioskop                 : "+nama);
        System.out.println("\nSaldo Kas               : "+ saldo);
        System.out.println("Jumlah tiket tersedia   : "+ tiket.size());
        System.out.print("Daftar Film tersedia    : ");
        for(int a=0; a<film.length;a++) {
            if (a == film.length - 1)
                System.out.print(film[a].getJudul());
            else {
                System.out.print(film[a].getJudul() + ", ");
            }
        }
        System.out.print("\n------------------------------------------------------------------\n");
    }

    public static void printTotalRevenueEarned(Theater[] bioskop){

        int sum=0;
        for(int a=0;a<bioskop.length;a++){
            sum+=bioskop[a].getSaldo();
        }
        System.out.println("Total uang yang dimiliki Koh Mas : Rp. "+ sum+
                "\n------------------------------------------------------------------");
        for (int a=0; a<bioskop.length;a++) {
            System.out.println("Bioskop     : " + bioskop[a].nama+
                                "\nSaldo Kas   : Rp. "+bioskop[a].getSaldo()+"\n");
        }
        System.out.println("------------------------------------------------------------------");
        System.out.println();
        }
}
