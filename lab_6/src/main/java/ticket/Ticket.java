package ticket;
import movie.Movie;

public class Ticket {
    private Movie film;
    private String hari;
    private boolean jenis;

    public Ticket(Movie film, String hari, boolean jenis){
        this.film =film;
        this.hari=hari;
        this.jenis=jenis;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

    public boolean isJenis() {
        return jenis;
    }

    public void setJenis(boolean jenis) {
        this.jenis = jenis;
    }

    public Movie getMovie(){
        return film;
    }


}
