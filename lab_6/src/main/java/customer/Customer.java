package customer;
import ticket.Ticket;
import movie.Movie;
import theater.Theater;


public class Customer {
    private String nama;
    private int umur;
    private String kelamin;

    public Customer(String nama, String kelamin, int umur){
        this.nama=nama;
        this.kelamin=kelamin;
        this.umur=umur;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUmur() {
        return umur;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    public String isKelamin() {
        return kelamin;
    }

    public void setKelamin(String kelamin) {
        this.kelamin = kelamin;
    }

    public Movie cekMovie(Theater bioskop, String judul){
        for (Movie i: bioskop.getFilm()) {
            if (i.getJudul().equals(judul)) {
                return i;
            }
        }
        return null;
    }

    public Ticket cekJadwal(Theater bioskop, String judul, String hari, String jenis) {
        boolean threedee=false;
        if(jenis.equals("3 Dimensi"))
            threedee=true;
        else{
            threedee=false;
        }

        for (Ticket i : bioskop.getTicket()) {
            if (i.getMovie().getJudul().equals(judul) && i.getHari().equals(hari) && i.isJenis()==threedee) {
                return i;
            }
        }
        return null;
    }

    public boolean cekRating(Movie film, int umur){
        if(film.getRating().equals("Umum"))
            return true;
        else if(film.getRating().equals("Remaja"))
            return umur > 13;
        else{
            return umur > 17;
        }

    }

    public int hitungHarga(String hari, String jenis){
        int harga = 60000;
        if (hari.equals("Sabtu")||hari.equals("Minggu")){
            harga += 40000;
        }
        if(jenis.equals("3 Dimensi")){
            harga=harga+(harga/5);
        }
        return harga;
    }

    public Ticket orderTicket(Theater bioskop, String judul, String hari, String jenis){
        Movie film = cekMovie(bioskop, judul);
        Ticket tiket= null;
        if(film!=null){
            tiket = cekJadwal(bioskop,judul,hari,jenis);
            if(tiket!=null){
                if(cekRating(film, umur)){
                    int harga = hitungHarga(hari, jenis);
                    System.out.println(nama+" telah membeli tiket "+ film.getJudul()+" jenis "+ jenis+" di "+bioskop.getNama()+" pada hari "+ hari +" seharga Rp. "+harga);
                    bioskop.setSaldo(bioskop.getSaldo()+harga);
                }
                else{
                    System.out.println(nama+" masih belum cukup umur untuk menonton "+film.getJudul()+ " dengan rating "+ film.getRating());
                }
            }
            else{
                System.out.println("Tiket untuk film "+film.getJudul()+" jenis "+jenis+" dengan jadwal "+hari+" tidak tersedia di "+bioskop.getNama());
            }

        }
        else{
            System.out.println("Tiket untuk film "+film.getJudul()+" jenis "+jenis+" dengan jadwal "+hari+" tidak tersedia di "+bioskop.getNama());
        }
        return tiket;
    }

    public void findMovie(Theater bioskop, String judul){
        Movie film=cekMovie(bioskop,judul);
        if(film!=null){
            System.out.println("------------------------------------------------------------------" +
                    "\nJudul   : "+judul +
                    "\nGenre   : "+film.getGenre() +
                    "\nDurasi  : "+film.getDurasi()+" menit" +
                    "\nRating  : "+film.getRating()+
                    "\nJenis   : Film "+film.getJenis()+
                    "\n------------------------------------------------------------------");
        } else {
            System.out.println("Film "+ judul+" yang dicari "+nama+" tidak ada di bioskop "+bioskop.getNama());
        }
    }

}
