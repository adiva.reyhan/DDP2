import java.util.Scanner;

public class Demo{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);

		System.out.print("Masukkan angka yang anda inginkan: ");
		String angka = input.nextLine();

		int angkaInt= Integer.parseInt(angka);
		System.out.println("integer dari " + angka + " adalah " + angkaInt);

		Long angkaLong=Long.parseLong(angka);
		System.out.println("long dari " + angka + " adalah " + angkaLong);

		Double angkaDouble=Double.parseDouble(angka);
		System.out.println("double dari " + angka + " adalah " + angkaDouble);

		input.close();
	}
}