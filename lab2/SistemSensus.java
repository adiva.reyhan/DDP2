import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Adiva Reyhan Puteri, NPM 1706984505, Kelas DDP2-C, GitLab Account: adiva.reyhan
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);

		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   		: ");
		String nama = input.nextLine();
		if (nama.length()==0){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Alamat Rumah           		: ");
		String alamat = input.nextLine();
		if (alamat.length()==0){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Panjang Tubuh (cm)     		: ");
		int panjang = Integer.parseInt(input.nextLine());
		//memeriksa validasi input user
		if ((panjang<=0)||(panjang>250)){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);}
		System.out.print("Lebar Tubuh (cm)      		: ");
		int lebar = Integer.parseInt(input.nextLine());
		if ((lebar<=0)||(lebar>250)){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);}
		System.out.print("Tinggi Tubuh (cm)		: ");
		int tinggi = Integer.parseInt(input.nextLine());
		if ((tinggi<=0)||(tinggi>250)){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);}
		System.out.print("Berat Tubuh (kg)       		: ");
		float berat = Float.parseFloat(input.nextLine());
		if ((berat<=0)||(berat>150)){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);}
		System.out.print("Jumlah Anggota Keluarga 	: ");
		int jumlahKeluarga = Integer.parseInt(input.nextLine());
		if ((jumlahKeluarga<=0)||(jumlahKeluarga>20)){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);}
		System.out.print("Tanggal Lahir(dd-mm-yyyy)	: ");
		String tanggalLahir = input.nextLine();
		int tahun = Integer.parseInt(tanggalLahir.substring(tanggalLahir.lastIndexOf("-")+1));
		if (tahun<=1000 || tahun>=2018){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Catatan Tambahan       		: ");
		String catatan = input.nextLine();
		System.out.print("Jumlah Cetakan Data   		: ");
		int jumlahCetakan = Integer.parseInt(input.nextLine());
		if (jumlahCetakan<1 || jumlahCetakan>99){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)

		int rasio = (int)((berat)/(panjang*lebar*tinggi*0.000001));

		for (int a=1; a<(jumlahCetakan+1); a++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("\nPencetakan " + a + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			String hasil = "DATA SIAP DICETAK UNTUK " + penerima + "\n"+
			"-----------------\n"+ nama +" - " + alamat + "\n" + "Lahir pada tanggal " + tanggalLahir +
			"\n" + "Rasio Berat Per volume  = "+ rasio + "kg/m^3";
			System.out.println(hasil);
			//TODO periksa ada catatan atau tidak
			if (catatan.length()!=0){
				System.out.println("Catatan: "+ catatan);
			}
			else{
				System.out.println("Tidak ada catatan tambahan");
			}
		}

		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
		// melakukan iterasi untuk menghitung jumlah ascii dan menyusun nomor keluarga
		int jumlahAscii=0;
		for (int b=0; b<nama.length(); b++){
			int ascii=(int) nama.charAt(b);
			jumlahAscii+=ascii;}
		String nomor=Integer.toString(((panjang*tinggi*lebar) + jumlahAscii) % 10000);

		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga=(nama.charAt(0))+nomor;

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaranMakanan = (int) (50000*365*jumlahKeluarga);

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		int tahunLahir = Integer.parseInt(tanggalLahir.substring(tanggalLahir.lastIndexOf("-")+1));
		int umur = (int) (2018-tahunLahir);

		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
		String namaApt="";
		String kabupaten="";
		String rangeUmur="";
		String rangeMakanan="";
		if (umur>=0 && umur<=18){
			namaApt="PPMT";
			kabupaten="Rotunda";
			rangeMakanan="BEBAS";
		}
		else if (umur>=19 && umur<=1018){
			if (anggaranMakanan>=0 && anggaranMakanan<=100000000){
				namaApt="Teksas";
				kabupaten="Sastra";
				rangeMakanan=Integer.toString(anggaranMakanan);
			}
			else{
				namaApt="Mares";
				kabupaten="Margonda";
				rangeMakanan=Integer.toString(anggaranMakanan);
			}
		}

		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		String rekomendasi = "\nREKOMENDASI APARTEMEN\n"+"--------------\n"+
						"MENGETAHUI: Identitas keluarga: "+nama+"-"+nomorKeluarga+
						"\nMENIMBANG: Anggaran makanan tahunan: Rp " + anggaranMakanan
						+ "\n           Umur kepala keluarga: " + umur + "tahun\n"+
						"MEMUTUSKAN: keluarga " + nama + " akan ditempatkan di:\n"+
						namaApt+", kabupaten "+ kabupaten;
		System.out.println(rekomendasi);

		input.close();
	}
}